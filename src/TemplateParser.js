import Handlebars from 'handlebars';
import klawSync from 'klaw-sync';
import path from 'path';

// handlebars templates filter
const allTemplatesFilter = item => path.extname(item.path) === '.hbs';

/**
 * TemplateParser class
 *
 * Parse and populate templates
 */
class TemplateParser {


    constructor(fileUtils)
    {
        this.fileUtils = fileUtils;
    }

    /**
     * Gets all templates from /templates
     * @returns {*}
     */
    getAllTemplates() {
        return klawSync(this.fileUtils.getCurrentDir() + '/../templates', { filter: allTemplatesFilter })
    }

    /**
     * Populates template with the given data
     *
     * @param templatePath
     * @param data
     */
    compileTemplate(templatePath, data) {
        let source = this.fileUtils.readFile(templatePath);
        let template = Handlebars.compile(source);
        return template(data);
    }

    /**
     * Get the correct output path
     * @param templatePath
     * @param config
     */
    getTemplateOutputPath(templatePath, config) {
        if(path.basename(templatePath).indexOf('CrudController') > -1) {
            return templatePath.replace('CrudController', config.controller.name);
        }
        if(path.basename(templatePath).indexOf('Model') > -1) {
            return templatePath.replace('Model', config.mongo.model);
        }
        return templatePath;
    }
}

export default TemplateParser;