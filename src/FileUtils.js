import fs from 'fs';
import path from 'path';

/**
 * FileUtils class
 *
 * Helper methods to handle files
 */
class FileUtils {

    /**
     * Creates file structure to dump files
     * @param outputDir
     */
    createFileStructure(outputDir) {
        const dirs = ['/src', '/src/controller', '/src/model', '/src/validator', '/config', '/dist', '/docker', '/test'];
        const output = this.getCurrentDir() + '/../' + outputDir;
        this.createDir(output);
        dirs.forEach((currentDir) => {
            this.createDir(output + currentDir);
        });
    }
    /**
     * Get file basename from path
     * @param templatePath
     * @param apiDir
     */
    getOuputDir(templatePath, apiDir) {
        return templatePath.replace('templates', apiDir).replace('.hbs', '');
    }

    /**
     * Get anem from config file path
     *
     * @param configFilePath
     */
    getNameFromConfigFile(configFilePath) {
        return path.basename(configFilePath).replace('.json', '');
    }

    /**
     * Get current directory
     *
     * @returns {*}
     */
    getCurrentDir() {
        return path.resolve(__dirname);
    }

    /**
     * Check if a file exists
     * @param filePath
     */
    fileExists(filePath) {
        return fs.existsSync(filePath);
    }

    /**
     * Create a new dir
     *
     * @param dirPath
     */
    createDir(dirPath) {
        if (!fs.existsSync(dirPath)){
            fs.mkdirSync(dirPath);
        }
    }

    /**
     * Create file in the given path with the given contents
     *
     * @param filepath
     * @param contents
     */
    createFile(filepath, contents) {
        // create dir if it does not exist
        this.createDir(path.dirname(filepath));
        fs.writeFileSync(filepath, contents);
        console.log("File created: " + filepath);
    }

    /**
     * Read file from disk
     * (we use it to read handlebars templates to compile them)
     *
     * @param filepath
     * @returns {*}
     */
    readFile(filepath) {
        return fs.readFileSync(filepath, 'utf8');
    }
}

export default FileUtils;