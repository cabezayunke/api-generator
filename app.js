#!/usr/bin/env node

// color output
import chalk from 'chalk';
// clear terminal
import clear from 'clear';
// command line tables and spinners
import {Spinner, CLI} from 'clui';
// ASCII art from text
import figlet from 'figlet';
// parses arguments
import minimist from 'minimist';

import FileUtils from './src/FileUtils';
import TemplateParser from './src/TemplateParser';
import ConfigValidator from './src/ConfigValidator';

const fileUtils = new FileUtils();
const configValidator = new ConfigValidator();
const templateParser = new TemplateParser(fileUtils);

// clear console
clear();
// show title
console.log(
    chalk.yellow(
        figlet.textSync('API-Generator', { horizontalLayout: 'full' })
    )
);

let status = new Spinner();
status.start();

// read and validate params
const argv = minimist(process.argv.slice(2));
const configFile = argv._[0];
status.message("Creating project from config " + configFile);

if(!configFile) {
    console.log("Missing params. Please use 'npm start path/to/config.json'");
    process.exit(1);
}
//check config file
if(!fileUtils.fileExists(configFile)) {
    console.error("Config file does not exist");
    process.exit(2);
}
const config = require(configFile);
if(!configValidator.isValidConfig(config)) {
    console.error("Invalid config. Please check the contents of " + configFile);
    process.exit(3);
}
// crete output dir
const outputDir = 'output/' + fileUtils.getNameFromConfigFile(configFile);
console.log('Output dir: ' + outputDir);
fileUtils.createDir(outputDir);

// create file structure
fileUtils.createFileStructure(outputDir);

let templates = templateParser.getAllTemplates();
status.message("Compiling templates...");
templates.forEach((template) => {
    let templatePath = templateParser.getTemplateOutputPath(template.path, config);
    let outputFile = fileUtils.getOuputDir(templatePath, outputDir);
    //compile template
    let compiledTemplate = templateParser.compileTemplate(
        template.path, // handlebars template path
        config // config file data
    );
    // save it to file
    fileUtils.createFile(
        outputFile, //
        compiledTemplate
    );
});

// done
status.stop();
console.log(chalk.green("All done"));